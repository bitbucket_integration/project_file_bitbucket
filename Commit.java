package maven_git;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.RemoteAddCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.errors.NotSupportedException;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.transport.URIish;

public class Commit {
	public static void main(String[] args) throws IOException,GitAPIException, URISyntaxException 
	   {
		Commit c=new Commit();
		   
		    String repoUrl = "https://bitbucket_integration@bitbucket.org/bitbucket_integration/javafile.git";
	    	//String username="zest.trainee011@gmail.com";
	    	//String password="cygnet@011";
	    	String gitdirectory = "C:\\Users\\niraj_000\\eclipse-workspace\\maven_git\\src\\main\\java\\maven_git";
	    	String msg="commit by nupur";
			c.Commit1(gitdirectory,repoUrl,msg);
		
			
		 }
	final static Logger logger=Logger.getLogger(Commit.class);
	
	public Response Commit1 (String gitdirectory, String repoUrl, String msg) throws IOException, GitAPIException, URISyntaxException
	{
          Git git = null;
       
  try {
	    PropertyConfigurator.configure("log4j.properties");
		BasicConfigurator.configure();
      	git = Git.open(new File(gitdirectory));
	 	RemoteAddCommand remoteAddCommand = git.remoteAdd();
	 	remoteAddCommand.setName("origin");
		remoteAddCommand.setUri(new URIish(repoUrl));
		remoteAddCommand.call();
		
        git.add().addFilepattern(".").call();
     
        
	    git.commit().setMessage(msg).call();
	    logger.info(Success.VALID_COMMIT);
        return new Response(Success.VALID_COMMIT);
        
  	} catch (RepositoryNotFoundException rne) {
   	   // System.out.println("Repository not found or it is not git directory");
  		logger.error(Error.INVALID_GIT_REPO);
  		return new Response(Error.INVALID_GIT_REPO);
  	}  catch (TransportException ue) {
		//System.out.println("invalid remote url and username ,password");
  		logger.error(Error.INVALID_URl);
  		return new Response(Error.INVALID_URl);
	}  catch (NotSupportedException ne) {
		logger.error(Error.URI_NOTSUPPORTED);
		return new Response(Error.URI_NOTSUPPORTED);
		
	}
 
System.out.println("hello");
}

}
