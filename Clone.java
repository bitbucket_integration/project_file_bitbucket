package maven_git;


import java.io.IOException;

//import java.io.BufferedReader;

import java.nio.file.Paths;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class Clone{

	
	final static Logger logger=Logger.getLogger(Clone.class);
	
	    	
	    	public Response CloneRepo(String repoUrl, String cloneDirectoryPath, String user, String password) throws IOException,GitAPIException
	    	{
	    			
	    	try {
	    		
	    		PropertyConfigurator.configure("log4j.properties");
	    		BasicConfigurator.configure();
	    	    System.out.println("Cloning "+repoUrl+" into "+repoUrl);
	    	    Git.cloneRepository()
	    	        .setURI(repoUrl)
	    	        .setDirectory(Paths.get(cloneDirectoryPath).toFile())
	    	        .setCredentialsProvider( new UsernamePasswordCredentialsProvider(user,password ) )
	    	        .call();
	    	    logger.info(Success.SCCESSFUL_CLONE);
	    	    return new Response(Success.SCCESSFUL_CLONE);	    
	    	    
	    	}
	    	 
	    	catch (InvalidRemoteException ire) {
	       		logger.error(Error.INVALID_URl);
	    		return new Response(Error.INVALID_URl);
	    	}  
	    	catch(JGitInternalException jge) {
	    		logger.error(Error.INVALID_LOCALREPO);
	    		return new Response(Error.INVALID_LOCALREPO);
	    	}catch(TransportException te) {
	    		logger.error(Error.INVALID_URl);
	    		return new Response(Error.INVALID_USER);
	    	}
	
	    }
	    	
	   	
	    
	   
}



