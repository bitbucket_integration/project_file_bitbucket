package maven_git;

public class Response extends Exception {

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int errorCode;
	  private String errorMsg;

	  public Response(Error code) {
	    this.errorMsg = code.getMsg();
	    this.errorCode = code.getId();
	  }
	  public Response(Success code) {
		    this.errorMsg = code.getMsg();
		    this.errorCode = code.getId();
		  }
	  public int getErrorCode() {
	    return errorCode;
	  }

	  public String getErrorMsg() {
	    return errorMsg;
	  }
	}