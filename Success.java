package maven_git;

	public enum Success {
		  SCCESSFUL_CLONE(201, "Cloneing Sccessfuly"),
		  SCCESSFUL_INIT(202, "Initialized empty Git repository"),
		 VALID_CLONE(100,"Successfully Cloned"),
		    VALID_PULL(200,"Successfully Pulled"),
		    VALID_COMMIT(400,"Succesfully Commit"),
		    VALID_PUSH(500,"Successfully Push"),
		    VALID_FETCH(600,"Successfully Fetch");
		  private final int id;
		  private final String msg;

		  Success(int id, String msg) {
		    this.id = id;
		    this.msg = msg;
		  }

		  public int getId() {
		    return this.id;
		  }

		  public String getMsg() {
		    return this.msg;
		  }
		  public String toString() {
			    return id + ": " + msg;
		  }
		 }